package app.fedilab.android.services;
/* Copyright 2021 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.unifiedpush.android.connector.MessagingReceiver;
import org.unifiedpush.android.connector.MessagingReceiverHandler;


import app.fedilab.android.client.Entities.Account;
import app.fedilab.android.helper.ECDH;
import app.fedilab.android.helper.NotificationsHelper;
import app.fedilab.android.helper.PushNotifications;
import app.fedilab.android.sqlite.AccountDAO;
import app.fedilab.android.sqlite.Sqlite;


class handler implements MessagingReceiverHandler {


    @Override
    public void onMessage(@Nullable Context context, @NotNull String s, @NotNull String slug) {

        new Thread(() -> {
            if (context != null) {
                SQLiteDatabase db = Sqlite.getInstance(context.getApplicationContext(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
                String[] slugArray = slug.split("@");
                ECDH ecdh = ECDH.getInstance();
                //ecdh.uncryptMessage(context, s, slug);
                Account account = new AccountDAO(context, db).getUniqAccountUsernameInstance(slugArray[0], slugArray[1]);
                NotificationsHelper.task(context, account);
            }
        }).start();
    }

    @Override
    public void onNewEndpoint(@Nullable Context context, @NotNull String endpoint, @NotNull String slug) {
        if (context != null) {
            new PushNotifications()
                    .registerPushNotifications(context, endpoint, slug);
        }
    }

    @Override
    public void onRegistrationFailed(@Nullable Context context, @NotNull String s) {
    }

    @Override
    public void onRegistrationRefused(@Nullable Context context, @NotNull String s) {
    }

    @Override
    public void onUnregistered(@Nullable Context context, @NotNull String s) {
    }
}

public class UnifiedPushService extends MessagingReceiver {
    public UnifiedPushService() {
        super(new handler());
    }
}
