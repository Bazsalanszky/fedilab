/* Copyright 2021 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.asynctasks;

import android.content.Context;

import java.lang.ref.WeakReference;

import app.fedilab.android.client.API;
import app.fedilab.android.client.APIResponse;
import app.fedilab.android.client.Entities.Account;


public class PostSubscriptionAsyncTask {

    private final WeakReference<Context> contextReference;
    private APIResponse apiResponse;
    private final String endpoint;
    private final Account account;

    public PostSubscriptionAsyncTask(Context context, Account account, String endpoint) {
        this.contextReference = new WeakReference<>(context);
        this.endpoint = endpoint;
        this.account = account;
        doInBackground();
    }


    protected void doInBackground() {
        new Thread(() -> {
            apiResponse = new API(contextReference.get(), account.getInstance(), account.getToken()).pushSubscription(endpoint, account);
        }).start();
    }

}
