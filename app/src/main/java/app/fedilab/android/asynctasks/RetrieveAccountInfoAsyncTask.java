/* Copyright 2017 Thomas Schneider
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.android.asynctasks;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.lang.ref.WeakReference;

import app.fedilab.android.client.API;
import app.fedilab.android.client.Entities.Account;
import app.fedilab.android.interfaces.OnRetrieveAccountInterface;

/**
 * Created by Thomas on 04/06/2017.
 * Verify credential
 */

public class RetrieveAccountInfoAsyncTask {


    private final OnRetrieveAccountInterface listener;
    private final WeakReference<Context> contextReference;
    private Account account;
    private API api;

    public RetrieveAccountInfoAsyncTask(Context context, OnRetrieveAccountInterface onRetrieveAccountInterface) {
        this.contextReference = new WeakReference<>(context);
        this.listener = onRetrieveAccountInterface;
        doInBackground();
    }

    protected void doInBackground() {

        new Thread(() -> {
            api = new API(this.contextReference.get());
            account = api.verifyCredentials();
            Handler mainHandler = new Handler(Looper.getMainLooper());
            Runnable myRunnable = () -> listener.onRetrieveAccount(account, api.getError());
            mainHandler.post(myRunnable);
        }).start();
    }

}