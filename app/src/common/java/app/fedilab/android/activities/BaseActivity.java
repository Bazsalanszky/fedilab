package app.fedilab.android.activities;

import android.annotation.SuppressLint;

import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.one.EmojiOneProvider;

import app.fedilab.android.helper.Helper;


/**
 * Created by Thomas on 12/12/2017.
 * Base activity which updates security provider
 */

@SuppressLint("Registered")
public class BaseActivity extends AllBaseActivity {

    static {
        Helper.installProvider();
        EmojiManager.install(new EmojiOneProvider());
    }

}
