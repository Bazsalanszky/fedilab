Changed:
- Improve memory management
- Improve scroll
- Poll layouts cleaner
- One logout entry in the menu (it will remove the account from the app)
- Clear push notifications when visiting notifications tab

Fixed:
- Long press to store media download the preview image
- Fix pagination with Nitter timelines
- Avatars not displayed for Peertube